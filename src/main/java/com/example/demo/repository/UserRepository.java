package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;
    public boolean block= false;
    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }
    public boolean isBlock() {
        return block;
    }
    public boolean checkLogin(final String login, final String password) {
        for(Integer i : usersDatabase.keySet()) {
            if (usersDatabase.get(i).getLogin().equals(login) ) {
                if(!usersDatabase.get(i).isActive()){
                    block = true;
                }}
                if(usersDatabase.get(i).getPassword().equals(password)){
                    return true;
                }
            else{
                usersDatabase.get(i).setIncorrectLoginCounter(usersDatabase.get(i).getIncorrectLoginCounter()+1);
                if(usersDatabase.get(i).getIncorrectLoginCounter() >= 2 ){
                    usersDatabase.get(i).setActive(false);
                }
            }
        }

            return false;
    }
}
